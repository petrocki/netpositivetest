<?php

namespace App\Controller;

use Abraham\TwitterOAuth\TwitterOAuth;
use Symfony\Bridge\Twig\Attribute\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Api as Api;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class DefaultController extends AbstractController
{
    #[Route('/', name: 'app_default')]
    public function index(Request $request)
    {
        return $this->redirectToRoute('app_view_posts', [
            'handle1' => 'knplabs', 
            'handle2' => 'symfony',
            'method' => 'fib'
        ]);
    }
    
    #[Route('/{handle1}/{handle2}/{method}', name: 'app_view_posts')]
    #[Template('default/index.html.twig')]
    public function viewPosts(Request $request, HttpClientInterface $httpClient, string $handle1, string $handle2, string $method = 'fib')
    {
        $posts = [];
        $error = null;
        try {
             if ($handle1 == $handle2) throw new \InvalidArgumentException('Handle1 and handle2 can\'t be the same');
            $twitter = new TwitterOAuth($_ENV['TWITTER_API_KEY'], $_ENV['TWITTER_API_KEY_SECRET'], $_ENV['TWITTER_ACCESS_TOKEN'], $_ENV['TWITTER_ACCESS_TOKEN_SECRET']);
            $twitter->setApiVersion('2');
            $api_service = new Api\ApiService();
            $api_service
                ->addContentApi(new Api\TwitterApiClient($twitter, $handle1), 20)
                ->addContentApi(new Api\TwitterApiClient($twitter, $handle2), 20);
            $api_service->setAdsApi(new Api\ICNDBApiClient($httpClient));

            $posts = $api_service->getPosts($method, true);
        } catch (\Exception $e) {
            $error = $e->getMessage();
        }
       
        return [
            'posts' => $posts,
            'error' => $error
        ];
    }
}
