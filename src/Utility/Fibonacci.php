<?php 

namespace App\Utility;

class Fibonacci {
    public static function isPerfectSquare($x): bool
    {
        $s = (int)(sqrt($x));
        return ($s * $s == $x);
    }

    public static function isFibonacci($n): bool
    {
        // n is Fibonacci if one of
        // 5*n*n + 4 or 5*n*n - 4 or
        // both is a perfect square
        return self::isPerfectSquare(5 * $n * $n + 4) || self::isPerfectSquare(5 * $n * $n - 4);
    }
}