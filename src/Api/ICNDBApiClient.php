<?php 

namespace App\Api;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class ICNDBApiClient extends BaseApiClient {
    const API_SOURCE = 'icndb';
    private HttpClientInterface $httpClient;
    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }
    protected function requestPosts(int $number): array
    {
        $posts = [];
        for($i = 0; $i < $number; $i++)
        {
            $response = $this->httpClient->request('GET', 'https://api.chucknorris.io/jokes/random');
            $data = json_decode($response->getContent());
            $posts[] = new ApiPost($this->getSourceName(), null, (string)$data->value);
        }
        
        return $posts;
    }
}