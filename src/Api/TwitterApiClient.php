<?php 

namespace App\Api;

use Abraham\TwitterOAuth\TwitterOAuth;

class TwitterApiClient extends BaseApiClient {
    const API_SOURCE = 'twitter';
    private $handle;
    private TwitterOAuth $twitter;
    
    public function __construct(TwitterOAuth $twitter, string $handle)
    {
        $this->twitter = $twitter;
        $this->handle = $handle;
    }

    public function getSourceName(): string
    {
        return static::API_SOURCE.'/'.$this->handle;
    }

    protected function requestPosts(int $number): array
    {
        $lookup_response = $this->twitter->get('users/by', ['usernames'=>[$this->handle]]);
        if (count($lookup_response->errors ?? []) > 0) throw new \InvalidArgumentException('Twitter API error: '.$lookup_response->errors[0]->detail);
        $id = $lookup_response->data[0]->id ?? null;
        if(!$id) throw new \RuntimeException('Twitter user id response is empty');
        
        $response = $this->twitter->get('users/'.$id.'/tweets', [
            'tweet.fields' => 'created_at,text',
            'max_results' => $number
        ]);

        $posts = [];
        foreach($response->data ?? [] as $tweet) {
            $posts[] = new ApiPost($this->getSourceName(), new \DateTime($tweet->created_at), $tweet->text);
        }
        
        return $posts;
    }
}