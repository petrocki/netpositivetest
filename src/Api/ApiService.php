<?php 

namespace App\Api;

use App\Utility\Fibonacci;

class ApiService {
    private array $content_apis = [];
    private array $post_nums = [];
    private ?BaseApiClient $ads_api;

    public function addContentApi(BaseApiClient $api, int $post_num): self
    {
        $this->content_apis[] = $api;
        $this->post_nums[] = $post_num;

        return $this;
    }

    public function setAdsApi(BaseApiClient $ads_api): self
    {
        $this->ads_api = $ads_api;

        return $this;
    }

    /**
     * Visszaadja a content api-kból kaapott posztokat, közbeszúrva az ads_api-ból kapott posztokkal 
     *
     * @param string $method ads_api posztok beszúrási módja. mod: minden harmadik, fib: fibonacci szám sorszámok
     * @param boolean $sort_desc posztok rendezése időpont szerint csökkenő sorrendbe?
     * @return array posztok
     */
    public function getPosts(string $method, bool $sort_desc = true): array
    {
        $available_methods = ['mod', 'fib'];
        if (!in_array($method, $available_methods)) throw new \InvalidArgumentException('Unknown method ' . $method);
        $posts = [];
        for($i = 0;$i < count($this->content_apis); $i++) {
            for($k = 0; $k < $this->post_nums[$i]; $k++) {
                $post = $this->content_apis[$i]->getPost($k);
                if (!$post) break;//kevesebb poszt van a feedben, mint amennyi a limit
                $posts[] = $post;
            }
        }

        //posztok rendezése idő szerint csökkenő sorrendbe
        usort($posts, function($a,$b) use ($sort_desc) {
            return ($a->getTime('U') - $b->getTime('U')) * ($sort_desc ? -1 : 1);
        });

        //ads_api posztok beszúrása
        $return = [];
        $ads_index = 0;
        for($i = 0; $i < count($posts); $i++) {
            $return[] = $posts[$i];
            if (($method == 'mod' && ($i + 1 + $ads_index) % 3 == 2) || ($method == 'fib' && $i + 1 >= 2 && Fibonacci::isFibonacci($i + 1 + $ads_index + 1))) {
                $return[] = $this->ads_api->getPost($ads_index);
                $ads_index++;
            }
        }

        return $return;
    }
}