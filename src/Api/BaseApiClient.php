<?php 

namespace App\Api;

abstract class BaseApiClient {
    const API_SOURCE = 'undefined';
    private ?array $posts = null;//postok, időrendben csökkenő sorrendben 

    public function getSourceName(): string
    {
        return static::API_SOURCE;
    }

    public function getPost(int $index): ?ApiPost
    {
    
        if ($this->posts == null) {
            $this->posts = $this->requestPosts(20);
        } 
        
        return $this->posts[$index] ?? null;

        //nincs meg a post, le kell kérni (az előtte lévőkkel együtt)
        
    }

    abstract protected function requestPosts(int $number): array;
}