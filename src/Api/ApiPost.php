<?php 

namespace App\Api;

class ApiPost {
    public function __construct(
        private string $source,
        private ?\DateTime $time,
        private string $message
    ) {}

    public function getSource(): string
    {
        return $this->source;
    }

    public function getTime($format = null): mixed
    {
        if (!$this->time) return null;
        if ($format) return $this->time->format($format);

        return $this->time;
    }

    public function getMessage(): string
    {
        return $this->message;
    }
}