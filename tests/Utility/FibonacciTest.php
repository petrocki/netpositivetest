<?php

namespace App\Tests\Utility;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use App\Utility\Fibonacci;

class FibonacciTest extends TestCase
{
    public function testIsFibonacci(): void
    {
        $fibo_numbers = [1,2,3,5,8,13,21,34,55,89,144,233];
        for($i=1;$i<300;$i++) {
            $is_fibo = Fibonacci::isFibonacci($i);
            $this->assertEquals(in_array($i, $fibo_numbers), $is_fibo);
        }
    }
}