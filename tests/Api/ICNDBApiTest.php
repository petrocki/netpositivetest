<?php

namespace App\Tests\Api;

use App\Api\ICNDBApiClient;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class ICNDBApiTest extends TestCase
{
    public function testSuccessfulRequest(): void
    {
        $responses = [];
        $text = 'Chuck Norris test #';
        for($i=1;$i<=20;$i++) {
            $responses[] = new MockResponse(json_encode([
                "icon_url" => "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
                "id" => $i,
                "url" => "",
                "value" => $text.$i
            ]));
        }
        
        $mockHttpClient = new MockHttpClient($responses);
        $api_client = new ICNDBApiClient($mockHttpClient);
        for($i=1;$i<=20;$i++) {
            $post = $api_client->getPost($i-1);
            $this->assertEquals($text.$i, $post->getMessage());
            $this->assertEquals(ICNDBApiClient::API_SOURCE, $post->getSource());
            $this->assertNull($post->getTime());
        }
    }
}
