<?php

namespace App\Tests\Api;

use App\Api\ApiPost;
use App\Api\ICNDBApiClient;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class ApiPostTest extends TestCase
{
    public function testApiPost(): void
    {
        $post = new ApiPost('testSource', new \DateTime(), 'test');
        $this->assertSame('test', $post->getMessage());
        $this->assertSame('testSource', $post->getSource());
    }
}
